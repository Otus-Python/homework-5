import argparse
import datetime
import errno
import locale
import mimetypes
import multiprocessing
import os
import signal
import socket
import threading
import urllib
from collections import namedtuple

ZERO = datetime.timedelta(0)
DATETIME_FORMAT = '%a, %d %b %Y %H:%M:%S %Z'


class GMT(datetime.tzinfo):
    """
    GMT timezone
    needs for using with datetime.strftime
    """

    def utcoffset(self, dt):
        return ZERO

    def tzname(self, dt):
        return 'GMT'

    def dst(self, dt):
        return ZERO


CRLF = '\r\n'
DOUBLE_CRLF = CRLF * 2

HTTP_OK = 200
HTTP_BAD_REQUEST = 400
HTTP_FORBIDDEN = 403
HTTP_NOT_FOUND = 404
HTTP_METHOD_NOT_ALLOWED = 405

HTTP_STATUS = {
    HTTP_OK: 'OK',
    HTTP_BAD_REQUEST: 'Bad Request',
    HTTP_FORBIDDEN: 'Forbidden',
    HTTP_NOT_FOUND: 'Not Found',
    HTTP_METHOD_NOT_ALLOWED: 'Method Not Allowed',
}

HTTP_METHODS = {'OPTIONS', 'GET', 'HEAD', 'POST',
                'PUT', 'PATCH', 'DELETE', 'TRACE', 'CONNECT'}

ALLOWED_HTTP_METHODS = {'HEAD', 'GET'}

HTTP_VERSIONS = {'HTTP/1.0', 'HTTP/1.1'}


def read_request(connection):
    """
    reads request from connection

    :param connection:
    :return:
    """
    request = ''
    while True:
        chunk = connection.recv(32)  # using a little value, just for test
        request += chunk
        if (DOUBLE_CRLF in request) or not chunk:
            break
    return request


Request = namedtuple('Request', 'method path protocol')


def parse_request(request):
    """
    extracts nesessary components from request
    query parameters and request headers are ignored

    :param request:
    :return:
    """
    lines = request.split(CRLF)
    method, path_with_params, protocol = lines[0].split()
    path_with_params = path_with_params.split('?')  # here we lost all the GET parameters
    path = path_with_params[0].strip('/')
    if protocol not in HTTP_VERSIONS:
        raise ValueError
    if method not in HTTP_METHODS:
        raise ValueError

    return Request(method, path, protocol)


File = namedtuple('File', 'content size mime')


def handle_path(path, document_root):
    """
    analyzes path and returns data for response
    or raises exception

    we don't define new Exception classes, so
    conventions is:
     - EnvironmentError - for permissions issues
     - NameError - for not found

    :param path:
    :param document_root:
    :return:
    """
    decoded_path = urllib.unquote(path).decode('utf-8')
    file_path = os.path.abspath(os.path.join(document_root, decoded_path))

    # if we have a directory path, add index file in path
    if os.path.isdir(file_path):
        file_path = os.path.abspath(os.path.join(file_path, 'index.html'))

    # check if path is outside of document root and raise exception
    if not file_path.startswith(document_root):
        raise EnvironmentError

    # if here is readable file on the path return data, else raise Exception
    if os.path.isfile(file_path):
        if os.access(file_path, os.R_OK):
            with open(file_path, 'rb') as f:
                content = f.read()
                size = os.fstat(f.fileno()).st_size
                mime = mimetypes.guess_type(file_path)[0]
            return File(content, size, mime)
        else:
            raise EnvironmentError
    else:
        raise NameError


def stringify_headers(headers):
    return CRLF.join('{}: {}'.format(k, v) for k, v in headers.iteritems())


def handle_request(client_connection, document_root, server_name):
    """
    task for thread.
    quite simple: thread reads request from connection,
    parses request, and prepares response

    :param client_connection:
    :param document_root:
    :param server_name:
    :return:
    """
    headers = {
        'Server': server_name,
        'Connection': 'closed'
    }

    target = None
    parsed_request = None
    http_response = ''
    try:
        raw_request = read_request(client_connection)
        parsed_request = parse_request(raw_request)
        if parsed_request.method not in ALLOWED_HTTP_METHODS:
            raise TypeError

        target = handle_path(parsed_request.path, document_root)
        headers['Content-Type'] = target.mime
        headers['Content-Length'] = target.size
        http_response = "HTTP/1.1 {} {}".format(HTTP_OK, HTTP_STATUS[HTTP_OK])

    except IOError as e:
        if e.errno == errno.EPIPE:
            pass  # SIGPIPE error happens when client already closed connection
    except ValueError:
        http_response = "HTTP/1.1 {} {}".format(HTTP_BAD_REQUEST, HTTP_STATUS[HTTP_BAD_REQUEST])
    except TypeError:
        http_response = "HTTP/1.1 {} {}".format(HTTP_METHOD_NOT_ALLOWED, HTTP_STATUS[HTTP_METHOD_NOT_ALLOWED])
    except NameError:
        http_response = "HTTP/1.1 {} {}".format(HTTP_NOT_FOUND, HTTP_STATUS[HTTP_NOT_FOUND])
    except EnvironmentError:
        http_response = "HTTP/1.1 {} {}".format(HTTP_FORBIDDEN, HTTP_STATUS[HTTP_FORBIDDEN])
    finally:
        headers['Date'] = datetime.datetime.now(GMT()).strftime(DATETIME_FORMAT),
        http_response = CRLF.join([http_response, stringify_headers(headers), CRLF])
        client_connection.sendall(http_response)
        if target is not None and parsed_request is not None and parsed_request.method == 'GET':
            client_connection.sendall(target.content)
        client_connection.close()


def prepare_socket(config):
    """
    just opens socket

    :param config:
    :return:
    """
    host, port, backlog = config['host'], config['port'], config['backlog']
    listen_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    listen_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    listen_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
    listen_socket.bind((host, port))
    listen_socket.listen(backlog)
    listen_socket.settimeout(5)  # just hardcoded
    return listen_socket


def serve_forever(listen_socket, config, exit_event):
    """
    task for process
    just listens for new client connection, accepts and passes it into new thread

    :param listen_socket:
    :param config:
    :return:
    """

    port = config['port']
    print 'Serving HTTP on port {} ...'.format(port)
    signal.signal(signal.SIGINT, signal.SIG_IGN)  # suppress exception
    while not exit_event.is_set():
        try:
            client_connection, client_address = listen_socket.accept()  # blocking socket
            client_connection.settimeout(config['timeout'])
            thread = threading.Thread(
                target=handle_request,
                args=(client_connection, config['document_root'], config['server'])
            )
            thread.start()
        except socket.timeout:
            pass  #
        except IOError as e:
            code, msg = e.args
            if code == errno.EINTR:
                continue
            else:
                raise

    listen_socket.close()
    print 'Stopping worker...'


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-w', '--workers', default=1, type=int, help='workers count')
    parser.add_argument('-r', '--document_root', required=True, help='path to document root')
    parser.add_argument('-i', '--ip', default='127.0.0.1', help='socket will be bind with this ip address')
    parser.add_argument('-p', '--port', default=8000, type=int, help='server will be listen for this port')
    parser.add_argument('-b', '--backlog', default=64, type=int, help='backlog parameter for connections pool')
    parser.add_argument('-s', '--server', default='Otus-Server', help='server name for server header in response')
    parser.add_argument('-t', '--timeout', default=20, type=int, help='client connection timeout in seconds')
    return parser.parse_args()


def get_config(args):
    return {
        'workers': args.workers,
        'document_root': os.path.abspath(args.document_root),
        'host': args.ip,
        'port': args.port,
        'backlog': args.backlog,
        'server': args.server,
        'timeout': args.timeout,
    }


if __name__ == '__main__':

    args = parse_args()
    config = get_config(args)
    locale.setlocale(locale.LC_ALL, 'C')

    workers = []

    listen_socket = prepare_socket(config)
    exit_event = multiprocessing.Event()
    for i in xrange(config['workers']):
        worker = multiprocessing.Process(
            target=serve_forever,
            args=(listen_socket, config, exit_event)
        )
        worker.daemon = True
        worker.start()
        workers.append(worker)

    try:
        for worker in workers:
            worker.join()
    except KeyboardInterrupt:
        exit_event.set()
        print("Shutting down server")
        for worker in workers:
            worker.join()
