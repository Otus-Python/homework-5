FROM python:2.7-alpine
WORKDIR /server
COPY . /server
EXPOSE 8000
CMD ["python", "httpd.py", "-i", "0.0.0.0", "-w", "4", "-r", "."]
