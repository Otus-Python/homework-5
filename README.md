# Otus Python. Week 5, Automatization (Simple Web Server)

Запуск сервера

```commandline
python httpd.py -w 4 -r ../http-test-suite
```

обязательные параметры:

* `-r` - абсолютный или относительный путь к document root

необязательные параметры:

* `-h`, `--help` - справка
* `-w`, `--workers` - количество процессов, `default=1`
* `-i`, `--ip` - ip к которому биндится socket. `default=127.0.0.1`. Если нужно чтобы сервер был
доступен с внешних ip (например в докере), нужно установить равным `0.0.0.0`
* `-p`, `--port` - порт на котором слушает сервер, `default=8000`
* `-b`, `--backlog` - размер пула соединений для сокета, `default=64`
* `-s`, `--server` - имя сервера, будет возвращаться клиенту в заголовке `Server`, `default=Otus-Server`
* `-t`, `--timeout` - таймаут для клиентского соединения в секундах, `default=20`

Запуск сервера в докере из прилагаемого докерфайла, правда смысла особо нет,
он умеет отдавать только / и /index.html
```commandline
docker build -t otus-server .
docker run --rm -it -p 8000:8000 otus-server
```

## Apache Benchmark results
```commandline
ab -n 50000 -c 100 -r http://127.0.0.1:8000/
This is ApacheBench, Version 2.3 <$Revision: 1807734 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking 127.0.0.1 (be patient)
Completed 5000 requests
Completed 10000 requests
Completed 15000 requests
Completed 20000 requests
Completed 25000 requests
Completed 30000 requests
Completed 35000 requests
Completed 40000 requests
Completed 45000 requests
Completed 50000 requests
Finished 50000 requests


Server Software:        Otus-Server
Server Hostname:        127.0.0.1
Server Port:            8000

Document Path:          /
Document Length:        169 bytes

Concurrency Level:      100
Time taken for tests:   4.594 seconds
Complete requests:      50000
Failed requests:        0
Total transferred:      15850000 bytes
HTML transferred:       8450000 bytes
Requests per second:    10884.64 [#/sec] (mean)
Time per request:       9.187 [ms] (mean)
Time per request:       0.092 [ms] (mean, across all concurrent requests)
Transfer rate:          3369.56 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    2  64.3      0    3040
Processing:     1    6   4.1      6     211
Waiting:        1    6   4.1      6     211
Total:          3    9  65.3      6    3249

Percentage of the requests served within a certain time (ms)
  50%      6
  66%      6
  75%      7
  80%      7
  90%      7
  95%      8
  98%     10
  99%     12
 100%   3249 (longest request)

```

